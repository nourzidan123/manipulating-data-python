from math import log
import matplotlib.pyplot as plt

checked_date = input("enter in format (yyyy-mm-dd)")

raw_data = open("owid-covid-data.csv","r")
points_list = []
for line in raw_data:
    strings_list=line.split(",")
    date, total_cases, total_deathes = strings_list[3], strings_list[4], strings_list[7]
    if date == checked_date:
        if total_deathes != "" and total_deathes != "0.0" and total_cases != "" and total_cases != "0.0":
            total_cases = float(total_cases)
            total_deathes = float(total_deathes)
            x = log(total_cases)
            y = log(total_deathes)
            point = (x, y)
            points_list.append(point)

x_list, y_list = [], []
for i in points_list:
    print("(x, y) =", i)
    x_list.append(i[0])
    y_list.append(i[1])

plt.plot(x_list, y_list, '*')

plt.show()

print("number of points: ", len(points_list))